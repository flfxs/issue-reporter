issue-reporter
==============

This is a simple script to retrieve a list of issues from the Bitbucket
issue tracker, and present them in a plaintext list that is readable to
a non-technical customer.


Example usage
-------------

Given a config file `$HOME/.issue-reporter.json` containing your
Bitbucket username and password:

    {
        "user": "my-username",
        "pass": "my-password"
    }

the following command can be issued from the terminal:

    issue-reporter accountname/reponame


which will result in a list of issues, sorted by one of three statuses:
to do, on hold, and done.


Example output
--------------

    TO DO
    - 0016: About: Create a box with bulleted list of strengths
    - 0015: About: Create a timeline
    - 0010: Contact: Allow use of imagery in each column/option
    - 0013: Customer reviews: Create a template
    - 0019: Home: show news slider from all social media (aggregated)
    - 0006: Misc: Use tags to create a subset of content suitable for SmartTV

    ON HOLD
    - 0005: Await feedback on necessary contact form fields

    DONE
    - 0012: Add user account for Valerie
    - 0007: Homepage blocks must have equal height
