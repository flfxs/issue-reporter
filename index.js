/*
 * Copyright (c) 2018 Flügelfoxes VOF
 */

'use strict';

const os = require('os');
const request = require('request');
const leftPad = require('left-pad');

const repo = getRepositoryFromArgs();
const conf = getConf();
const path = `https://${conf.user}:${conf.pass}@api.bitbucket.org/1.0/repositories/${repo}/issues`;
const issues = getSkeleton();
const requestOptions = {
  json: true,
};

request(path, requestOptions, (err, res, body) => {
  if (err) {
    console.log('There was an error.');
    console.log(err + '\n');
    process.exit(1);
  }

  if (body.type && body.type === 'error') {
    console.log('There was an error.');
    console.log(body.error.message + '\n');
    process.exit(1);
  }

  if (!body.issues.length) {
    console.log('No issues found.\n');
    process.exit(0);
  }

  body.issues.map((retrievedIssue) => {
    const issue = {
      id: leftPad(retrievedIssue.local_id, 4, 0),
      title: retrievedIssue.title,
      priority: retrievedIssue.priority,
      status: retrievedIssue.status,
    };

    switch (issue.status) {
      case 'on hold':
        issues['on hold'].push(issue);
      break;
      case 'resolved':
        issues['done'].push(issue);
      break;
      default:
        issues['to do'].push(issue);
    }
  });

  for (const issuesByStatus in issues) {
    if (issues.hasOwnProperty(issuesByStatus)) {
      if (!issues[issuesByStatus].length) {
        continue;
      }

      console.log(issuesByStatus.toUpperCase());

      issues[issuesByStatus].sort((a, b) => {
        return a.title.localeCompare(b.title);
      })

      issues[issuesByStatus].map((issue) => {
        console.log(` - ${issue.id}: ${issue.title}`);
      });
    }
  }
});

function getRepositoryFromArgs() {
  if (process.argv[2] === undefined) {
    console.log('Sorry.');
    console.log('You need to specify a repository: issue-reporter <account/repo>\n');
    process.exit(1);
  }
  return process.argv[2];
}

function getConf() {
  try {
    const conf = require(os.homedir() + '/.issue-reporter');
    return conf;
  } catch (Exception) {
    console.log('Sorry.');
    console.log('No $HOME/.issue-reporter.json found');
    console.log('This file should be a simple JSON object with "user" and "pass" key/value pairs.\n');
    process.exit(1);
  }
}

function getSkeleton() {
  return {
    'to do': [],
    'on hold': [],
    'done': [],
  };
}
